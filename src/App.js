import React from "react";
import Todo from "./components/TODO";

function App() {
  return (
    <div className="container">
      <div className="row">
        <div className="col-sm">
          <Todo />
        </div>
      </div>
    </div>
  );
}

export default App;
